package create.builder;

/**
 * 指挥者  收银员类 根据订单指挥 到底做什么样的面
 */
public class Cashier {
    public void BuidFood(Builder builder) {
        builder.AddTomattos();
        builder.AddEggs();
        builder.WaterTemperature();
        builder.CutTimesWheatflour();
        builder.Stir();
    }
}
