package create.builder;

public class Program {
    public static void main(String[] args) {
        //创建指挥者
        Cashier director = new Cashier();
        //创建普通鸡蛋面的制造者 小康师傅
        LittleMasterKang kang = new LittleMasterKang();
        director.BuidFood(kang);
        Food food = kang.BuildFood();
        food.ShowFood();

        //创建大份量鸡蛋面建造者 康师傅
        MasterKang k = new MasterKang();
        director.BuidFood(k);
        Food f = k.BuildFood();
        f.ShowFood();
    }
}
