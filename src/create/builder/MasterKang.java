package create.builder;

/**
 * 具体建造者 大师级的康师傅类 用来生产大份量的套餐
 */
public class MasterKang extends Builder {
    Food normalFood = new Food();

    @Override
    public void AddTomattos() {
        System.out.println("康师傅：切两个尖椒，咔咔.....放入锅中");
        normalFood.dicFood.put("尖椒", 2d);
    }
    @Override
    public void AddEggs() {
        System.out.println("康师傅：肉丝");
        normalFood.dicFood.put("肉丝", 3d);
    }

    @Override
    public void WaterTemperature() {
        System.out.println("康师傅：水温有100℃了，可以下面了");
        normalFood.dicFood.put("汤", 0d);
    }

    @Override
    public void CutTimesWheatflour() {
        System.out.println("康师傅：咔咔.....嗯，够了");
        normalFood.dicFood.put("面(2￥)",7d);
    }

    @Override
    public void Stir() {
        System.out.printf("康师傅：一圈....%d圈,够了\n", 60);
    }
    
    // 返回食物
    @Override
    public Food BuildFood() {
        return normalFood;
    }
}
