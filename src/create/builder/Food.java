package create.builder;

import java.util.LinkedHashMap;
import java.util.Iterator;

/**
 * 产品类Food
 */
public class Food {

    LinkedHashMap<String, Double> dicFood;     //面条名及价格
    private Double price=new Double(0);  //面条价格
    private String foodName="";                //面条名

    public Food() {
        dicFood = new LinkedHashMap<String, Double>();
    }

    public void ShowFood() {
        for(Iterator<String> itr = dicFood.keySet().iterator(); itr.hasNext();){
            String key = itr.next();
            Double value = dicFood.get(key);
            foodName = foodName + key ;
            price += value;
        }
        System.out.println(foodName + "\t￥" + price + "\n--------------------------");
    }

    public LinkedHashMap<String, Double> getDicFood() {
        return dicFood;
    }

    public void setDicFood(LinkedHashMap<String, Double> dicFood) {
        this.dicFood = dicFood;
    }
}
