package create.builder;

/**
 * 具体建造者  小康师傅类 负责普通鸡蛋面的生产
 */
public class LittleMasterKang extends Builder {
    Food normalFood = new Food();

    @Override
    public void AddTomattos() {
        System.out.println("小康师傅：切两个西红柿，咔咔.....放入锅中");
        normalFood.dicFood.put("西红柿", 2d);
    }

    @Override
    public void AddEggs() {
        System.out.println("小康师傅：鸡蛋");
        normalFood.dicFood.put("鸡蛋", 3d);
    }

    @Override
    public void WaterTemperature() {
        System.out.println("小康师傅：水温有100℃了，可以下面了");
        normalFood.dicFood.put("汤", 0d);
    }

    @Override
    public void CutTimesWheatflour() {
        System.out.println("小康师傅：咔咔.....嗯，够了");
        normalFood.dicFood.put("面", 5d);
    }

    @Override
    public void Stir() {
        System.out.printf("小康师傅：一圈....%d圈,嗯,够了\n", 50);
    }

    // 返回食物
    @Override
    public Food BuildFood() {
        return normalFood;
    }
}
