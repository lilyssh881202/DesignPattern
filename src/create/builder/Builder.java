package create.builder;

/**
 * 建造者 指定不变的创建过程（算法）
 */
public abstract class Builder {
    // 水温
    public abstract void WaterTemperature();

    // 添加西红柿
    public abstract void AddTomattos();

    // 添加鸡蛋
    public abstract void AddEggs();

    // 削多少面
    public abstract void CutTimesWheatflour();

    // 搅拌
    public abstract void Stir();

    // 返回
    public abstract Food BuildFood();
}
