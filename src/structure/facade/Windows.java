package structure.facade;

/// 外观类
public class Windows {
    //使用组合将使用到的子系统组件，全部都在这里
    KingSoft kingSoft;
    BaoFeng baoFeng;
    Kugou kugou;
    QQ qq;
    Service services;

    public Windows(KingSoft kingSoft, BaoFeng baoFeng, Kugou kugou, QQ qq, Service services) {
        //外观将子系统中每一个组件的引用都传入它的构造函数中，然后外观把它们赋值给相应的实例变量
        this.kingSoft = kingSoft;
        this.baoFeng = baoFeng;
        this.kugou = kugou;
        this.qq = qq;
        this.services = services;
    }

    public void StartWindows() {
        kingSoft.StartKingSoft();
        baoFeng.StartBaofeng();
        kugou.StartKugou();
        qq.StartQQ();
        services.StartService();
    }
}
