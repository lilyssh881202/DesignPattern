package structure.facade;

/**
 * 外观模式 （Facade Pattern）  结构型模式
 * 隐藏系统的复杂性，并向客户端提供了一个客户端可以访问系统的接口。
 * 这种模式涉及到一个单一的类，该类提供了客户端请求的简化方法和对现有系统类方法的委托调用。
 *
 * 示例：按下开机键，默认开机启动的软件就都启动了。
 */
public class Program {
    public static void main(String[] args) {
        KingSoft kingSoft = new KingSoft();
        BaoFeng baoFeng = new BaoFeng();
        Kugou kugou = new Kugou();
        QQ qq = new QQ();
        Service services = new Service();
        Windows windows = new Windows(kingSoft, baoFeng, kugou, qq, services);
        windows.StartWindows();
    }
}
