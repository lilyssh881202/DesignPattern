package behaviour.observer;

import java.util.ArrayList;
/**
 * 天气概况、注册中心、变更通知 接口  实现类
 */
public class WeatherData implements Subject {

    private ArrayList observers; //观察者。

    private float temperature;  //温度
    private float humidity;     //湿度
    private float pressure;     //气压

    public WeatherData() {
        observers = new ArrayList();
    }

    public void RegisterObserver(Observer o) {
        //当注册观察者的时候，秩序把他们加在ArrayList后面就行了
        observers.add(o);
    }

    public void RemoveObserver(Observer o) {
        //同样，当观察者想取消注册，只需要移除
        int i = observers.indexOf(o);
        if (i > 0) {
            observers.remove(i);
        }
    }

    // 把状态告诉观察者们。因为观察者都实现了Update方法，所以知道如何通知他们。
    public void NotifyObservers() {
        for (int i = 0; i < observers.size(); i++) {
            Observer observer = (Observer) observers.get(i);
            observer.update(temperature, humidity, pressure);
        }
    }

    // 当气象站得到更新观测值的时，我们通知观察者。
    public void MeasurementChanged() {
        NotifyObservers();
    }

    public void SetMeasurements(float temperature, float humidity, float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
    }
} 
