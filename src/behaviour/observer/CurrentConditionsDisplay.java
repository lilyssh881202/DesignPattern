package behaviour.observer;

/**
 * 观察者 布告板
 */
public class CurrentConditionsDisplay implements Observer, DisplayElement {
    private float temperature;  //温度
    private float humidity;     //湿度
    private Subject weatherData;    //天气概况、注册中心、变更通知

    public CurrentConditionsDisplay(Subject weatherData) {
        this.weatherData = weatherData;
        weatherData.RegisterObserver(this);     //注册观察者
    }

    public void update(float temp, float humidity, float pressure) {
        //当update被调用的时候，我们把温度和湿度保存起来，然后调用Display();
        this.temperature = temp;
        this.humidity = humidity;
        display();
    }

    // 只是将最近的湿度和温度显示出来。
    public void display() {
        System.out.println("Current conditions:" + temperature + " F degrees and" + humidity + "% humidity");
    }
}
