package behaviour.observer;

/**
 * 观察者模式（Observer Pattern） 行为型模式  也被称为：
 * 发布-订阅（Publish/Subscribe）模式，模型-视图（Model/View）模式，
 * 源-监听器（Source/Listener）模式，或从属者（Dependents）模式。
 * 当对象间存在一对多关系时，则使用观察者模式。
 *
 * 发起请求的客户端
 */
public class Program {
    public static void main(String[] args) {
        //天气概况、注册中心、变更通知实现类   初始化观察者集合
        WeatherData weatherData = new WeatherData();
        //观察者 布告板   注册观察者
        CurrentConditionsDisplay currentDisplay = new CurrentConditionsDisplay(weatherData);
        //设置温度、湿度、气压
        weatherData.SetMeasurements(80, 65, 30.4f);
        //通知 观察者 布告板
        weatherData.NotifyObservers();
        weatherData.SetMeasurements(82, 70, 29.2f);
        weatherData.NotifyObservers();
        weatherData.SetMeasurements(78, 90, 29.2f);
        weatherData.NotifyObservers();
    }
}
