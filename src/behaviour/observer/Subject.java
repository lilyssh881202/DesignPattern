package behaviour.observer;

/**
 * 注册中心、天气变更通知 接口
 */
public interface Subject {
    void RegisterObserver(Observer o);//这两个方法都需要一个观察者作为参数，该观察者是用来注册或被删除的。

    void RemoveObserver(Observer o);

    void NotifyObservers();//当主题改变状态时，这个方法会被调用，以通知所有的观察者
}
