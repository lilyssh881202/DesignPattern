package behaviour.observer;

/**
 * 观察者  当气象观测值改变时，主题会把这些状态值作为方法的参数传给观察者
 */
public interface Observer {
    void update(float temp, float humidity, float pressure);
}
