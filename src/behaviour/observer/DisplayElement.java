package behaviour.observer;

/**
 * 当布告板需要显示时，调用此方法。
 */
public interface DisplayElement {
    void display();
}
