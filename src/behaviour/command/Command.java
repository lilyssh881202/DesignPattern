package behaviour.command;
/**
 * 抽象命令
 */
public abstract class Command {
    public abstract void Execute();
}
