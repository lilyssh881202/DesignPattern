package behaviour.command;

/**
 * 命令的执行者 厨师
 */
public class SeniorChef {
    public void MakeFood(int num, String foodName) {
        System.out.printf("%d份%s\n",num,foodName);
    }
}
