package behaviour.command;

import java.util.ArrayList;

/**
 * 命令的调用者 服务员  持有命令 且可以触发
 */
public class Waitor {
    ArrayList commands = null;//可以持有很多的命令对象

    public Waitor() {
        commands = new ArrayList();
    }

    public void SetCommand(Command cmd) {
        commands.add(cmd);
    }

    // 提交订单 喊 订单来了，厨师开始执行
    public void OrderUp() {
        System.out.println("美女服务员：叮咚，大厨，新订单来了.......");
        System.out.println("资深厨师：收到\n---------");
        for (int i = 0; i < commands.size(); i++) {
            Command cmd = (Command) commands.get(i);
            if (cmd != null) {
                cmd.Execute();
            }
        }
    }
}

