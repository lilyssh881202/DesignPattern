package behaviour.command;

import java.util.HashMap;

/**
 * 点菜单 实体类
 */
public class Order {
    // 餐桌号码
    public int DiningTable;
    // food  key：饭名 value：多少份
    public HashMap<String, Integer> FoodDic;

    public int getDiningTable() {
        return DiningTable;
    }

    public void setDiningTable(int diningTable) {
        DiningTable = diningTable;
    }

    public HashMap<String, Integer> getFoodDic() {
        return FoodDic;
    }

    public void setFoodDic(HashMap<String, Integer> foodDic) {
        FoodDic = foodDic;
    }
}

