package behaviour.command;

/**
 * 具体的命令  持有接收者（厨师） 和  点菜单，可调用厨师的做饭方法
 */
public class OrderCommand extends Command {
    SeniorChef receiver;
    Order order;

    public OrderCommand(SeniorChef receiver, Order order) {
        this.receiver = receiver;
        this.order = order;
    }

    @Override
    public void Execute() {
        System.out.printf("%d桌的订单：\n", order.DiningTable);
        for (String item : order.getFoodDic().keySet()) {
            //通常会转调接收者对象的相应方法，让接收者来真正执行功能
            receiver.MakeFood(order.FoodDic.put(item, 1), item);
        }
        try {
            Thread.sleep(2000);//停顿一下 模拟做饭的过程
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.printf("%d桌的饭弄好了\n---------\n", order.DiningTable);
    }
}

