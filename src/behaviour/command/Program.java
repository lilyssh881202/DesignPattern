package behaviour.command;

import java.util.HashMap;

/**
 * 命令模式 (Command Pattern)
 * 请求以命令的形式包裹在对象中,并传给调用对象。
 * 调用对象寻找可以处理该命令的合适的对象,并把该命令传给相应的对象,该对象执行命令。
 * 示例：服务员写下点菜单，厨师去做菜。
 *
 * 此处：发起请求的客户端  构造命令（点餐） 设置命令并触发（服务员告知）
 */
public class Program {
    public static void main(String[] args) {
        //program类 作为客户端
        //创建2个order
        Order order1 = new Order();
        order1.DiningTable = 1;
        order1.FoodDic = new HashMap<String, Integer>();
        order1.FoodDic.put("西红柿鸡蛋面", 1);
        order1.FoodDic.put("小杯可乐", 2);

        Order order2 = new Order();
        order2.DiningTable = 3;
        order2.FoodDic = new HashMap<String, Integer>();
        order2.FoodDic.put("尖椒肉丝盖饭", 1);
        order2.FoodDic.put("小杯雪碧", 1);
        //创建接收者
        SeniorChef receiver = new SeniorChef();
        //将订单这个两个消息封装成命令对象
        OrderCommand cmd1 = new OrderCommand(receiver, order1);
        OrderCommand cmd2 = new OrderCommand(receiver, order2);
        //创建调用者 waitor
        Waitor invoker = new Waitor();
        //添加命令
        invoker.SetCommand(cmd1);
        invoker.SetCommand(cmd2);
        //将订单带到柜台 并向厨师喊 订单来了
        invoker.OrderUp();
    }
}

