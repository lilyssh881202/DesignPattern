package behaviour.state;

/**
 * 状态模式
 * 适用场景：当一个对象的行为取决于它的状态，并且它必须在运行时刻根据状态改变它的行为时，就可以考虑使用状态模式。
 * 示例：红绿灯
 */
public class Program {
    public static void main(String[] args) {
        TrafficLight trafficLight = new TrafficLight(2000);
        //初始化第一个状态
        trafficLight.setState(new RedState());
        //发送请求 显示第一个状态 不用等待
        trafficLight.request();
    }
}
