package behaviour.state;

import java.util.Timer;
import java.util.TimerTask;

//TrafficLight相当于 Context(上下文)类 拥有一些内部状态
public class TrafficLight {
    private Timer timer;    //定时器 为了模拟等待的过程 定义一个定时器
    private TrafficLightState state; //记录状态

    //构造函数(时间间隔)
    public TrafficLight(int time) {
        this.timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                request();
            }
        },1000, time);
    }

    public void setState(TrafficLightState state) {
        this.state = state;
    }

    //对请求做处理，并设置下一个状态
    public void request() {
        state.changeState(this);    //时间到 改变一次状态
    }
}
