package behaviour.state;

//具体状态类 绿灯状态
public class GreenState extends TrafficLightState {
    @Override
    public void changeState(TrafficLight contex) {
        System.out.println("绿灯行");
        contex.setState(new RedState());
    }
}
