package behaviour.state;

//具体状态类 黄灯状态
public class YellowState extends TrafficLightState {
    @Override
    public void changeState(TrafficLight contex) {
        System.out.println("黄灯亮了等一等");
        //改变状态
        contex.setState(new GreenState());
    }
}
