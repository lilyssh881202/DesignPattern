package behaviour.state;

//具体状态类 红灯状态
public class RedState extends TrafficLightState {
    @Override
    public void changeState(TrafficLight contex) {
        System.out.println("红灯停");
        //改变状态
        contex.setState(new YellowState());
    }
}
